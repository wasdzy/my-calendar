package cn.mrlong.mycalendar

import java.time.DayOfWeek
import java.time.LocalDate
import java.util.Date

data class DateBean(
    var calendar: LocalDate,
    var dayOfWeek: DayOfWeek,
    var holiday: String? = null,
    var weather: String? = null,
)