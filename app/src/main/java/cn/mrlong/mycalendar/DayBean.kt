package cn.mrlong.mycalendar

import java.time.Month
import java.time.Year

data class DayBean(
    var day: Int,
    var month: Int,
    var year: Int,
    var oldCalender: String? = null,
    var taboo: String? = null,
    var currentDay: Boolean? = false,
    var currentMount: Boolean? = false
)
