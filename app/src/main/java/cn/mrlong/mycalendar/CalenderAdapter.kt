package cn.mrlong.mycalendar

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.Recycler
import cn.mrlong.mycalendar.databinding.ItemBinding

class CalenderAdapter(
    var mContext: Context,
    var mDatas: ArrayList<DayBean> = ArrayList()
) :
    Adapter<CalenderAdapter.CalenderHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalenderHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item, parent, false)
        val holder = CalenderHolder(view);
        return holder
    }

    override fun getItemCount(): Int {
        return mDatas.size
    }

    override fun onBindViewHolder(holder: CalenderHolder, position: Int) {
        holder.setData(mDatas[position])
    }

    public fun updates(datas: List<DayBean>) {
        this.mDatas.clear()
        this.mDatas.addAll(datas)
        notifyDataSetChanged()
    }

    inner class CalenderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var binding: ItemBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }

        fun setData(dayBean: DayBean) {
            binding?.apply {
                idCalender.text = dayBean.day.toString()
                oldCalender.text = dayBean.oldCalender.toString()
                taboo.text = dayBean.taboo
                if (!dayBean.currentMount!!) {
                    idCalender.alpha = 0.4f
                } else {
                    idCalender.alpha = 1f
                }
                if (dayBean.currentDay!!) {
                    idCalender.setTextColor(Color.RED)
                } else {
                    idCalender.setTextColor(Color.WHITE)
                }
            }

        }
    }
}
